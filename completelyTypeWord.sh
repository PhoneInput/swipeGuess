#!/bin/sh

while read -r word; do
  if test -n "$word"; then
    wtype -P Backspace
    wtype "$word"
    wtype -P Space
  fi
done
