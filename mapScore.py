#!/usr/bin/env python3
import sys
import math

def makeMap(filename):
  l= [(0,0) for x in range(0,127)]
  with open(filename) as f:
    x=0;y=0
    while 1:
      c = f.read(1)
      if c == '\t':
        x=x+1
      elif c == '\n':
        y=y+1
        x = (y%2)/2
      elif c:
        l[ord(c)] =(x,y)
      else:
        break
  return l

def scoreWord(word,mm,fun):
  pc=word[0] or ' '
  s=0
  for c in word:
    s += fun(mm[ord(pc)],mm[ord(c)])
    pc=c
  return s

def manhattan_dist(p1, p2):
   return abs(int(p1[0])-int(p2[0])) + abs(p1[1]-p2[1])

def bee_dist(p1, p2):
   yd = abs(p1[1] - p2[1])
   xd = max(0, math.floor(abs(p1[0]-p2[0])) - yd//2)
   return xd+yd

def main(argv):
  fun = manhattan_dist
  if len(argv) >= 3 and argv[2] == 'bee':
    fun = bee_dist
  mm=makeMap(argv[1])
  while 1:
   line = sys.stdin.readline()
   if not line:
     break
   w=line.strip()
   if w:
    print(scoreWord(w,mm,fun),w,sep='\t')

if __name__ == '__main__':
  main(sys.argv)
