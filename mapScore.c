#include <stdio.h>
#include <stdlib.h>

#define BUFSIZE 1024
char buffer[BUFSIZE];
// TODO: Support UTF-8.
int map[256][2];

void makeMap(FILE *f) {
	int x = 0; int y = 0;
	int c = 0;
	while ((c = fgetc(f)) != EOF) {
		switch (c) {
		case '\t': x++; break;
		case '\n': y++; x=0; break;
		default:
			map[c][0] = x;
			map[c][1] = y;
			break;
		}
	}
}

int scoreWord(char *word, int (*fun)(int*,int*)) {
	char *c = word;
	int score = 0;
	for (c++;*c;c++) {
		if (*c == '\n') break;
		score += (*fun)(map[c[-1]],map[c[0]]);
	}
	return score;
}

int manhattanDist(int *p1,int *p2) {
	return abs(p1[0]-p2[0]) + abs(p1[1]-p2[1]);
}
int beeDist(int *p1, int *p2) {
	const int yd = p1[1] - p2[1];
	const int xd = p1[0] - p2[0];
	const int ayd = abs(yd);
	int axd = abs(xd);
	if (ayd % 2) {
		if ((p1[1]/2 + p2[1]/2) % 2) {
			if (xd * yd > 0) axd--;
		} else {
			if (xd * yd < 0) axd--;
		}
	}
	axd -= ayd / 2;
	if (axd < 0) axd = 0;
	return axd + ayd;
}

int main(int argc, char **argv) {
	if (argc < 2) {
		fprintf(stderr, "Usage: mapScore map.tsv <words.txt >scoredWords.tsv");
		return 1;
	}

int (*fun)(int*,int*) = &manhattanDist;
	if (argc >= 3) {
		fun = &beeDist;
	}

	FILE *f = fopen(argv[1], "r");
	makeMap(f);
	fclose(f);
	while (fgets(buffer, BUFSIZE, stdin)) {
		printf("%d\t%s", scoreWord(buffer, fun), buffer);
	}
}
