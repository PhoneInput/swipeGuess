# swipeGuess

swipeGuess is a completion program intended to be used as a plugin for touchscreen-keyboards on mobile devices.

For each line input from stdin, it looks through a wordlist and outputs the first possible match for that gesture.

it's run like `input-program | swipeGuess wordlist.txt | output-program`

## input-program

The input program should output a stream of letters "swiped through", then a newline.

This is supported by [wvkbd](https://github.com/proycon/wvkbd) since version 0.6.

## wordlist

This should be a list of words, sorted by preferability of choice.

Good starting points can be found on the web, based on your language.

### Example English Word Lists

* http://www-personal.umich.edu/~jlawler/wordlist - 69,903 English words, including obscure words, missing common stems.
* http://www.mieliestronk.com/corncob_lowercase.txt - 58,110 English words with British spelling.
* https://raw.githubusercontent.com/dwyl/english-words/master/words.txt - 466,550 words, including many proper nouns like names.
* https://norvig.com/ngrams/count_1w.txt - 333,333 words, abbreviations, etc, sorted by popularity.
* https://www.keithv.com/software/wlist/index.php - page with several links providing intersections between english word lists (26,680 - 1,516,998 words)

### sorting tips

The most basic sorting technique would put longest words first, otherwise, it may be impossible to type them.

```
# Good autocompletion for terminal
ls $PATH | awk '{ print length, $0 }' | sort -nr | cut -d" " -f2- > binsSorted.txt

```

Smarter sorting would be keyboard-layout aware. mapscore.py can do that for you

```sh
./mapScore.py map.tsv <words.txt | sort -nr | cut -f2 > wordsSorted.txt
```

map.tsv uses tabs and newlines to create the grid-based layout. See `map.qwerty.simplegrid.tsv` for a sample of how to format this file.

If your keys are in a hexagonal layout, use mapScore like
`./mapScore.py map.simple.tsv bee`.

## output-program

* `completelyTypeWord.sh` (included in this repository) will type the characters (besides the first character, which is already typed by wvkbd) using wtype.

# Installation/Usage with wvkbd

1. Be using a wayland-based graphical shell (such as sway)
2. Compile with your favorite C compiler: `gcc swipeGuess.c -o swipeGuess`.
2. copy swipeGuess completelyTypeWord.sh into your $PATH (`~/.local/bin/` or `/usr/local/bin/` for example)
3. `wvkbd-mobintl -O | swipeGuess /path/to/words.txt | completelyTypeWord.sh`
	* In SXMO, `KEYBOARD_ARGS='-O | swipeGuess /path/to/words.txt | completelyTypeWord.sh'` can be added to your ~/.profile to enable this (effective on restart).
