#!/bin/sh
cat ~/.local/share/sxmo/words.txt - > /tmp/unsortedwords.txt
mapScore ~/.local/share/sxmo/keyboard.map.tsv </tmp/unsortedwords.txt | sort -nr | cut -f2 > ~/.local/share/sxmo/words.txt
